# Manufacturing
The PCB [schematic](https://gitlab.com/beemolai12/eee3088f-pihat-project-g14/-/blob/main/PCB/Motor_Hat.sch) and [board](https://gitlab.com/beemolai12/eee3088f-pihat-project-g14/-/blob/main/PCB/Motor_Hat.kicad_pcb) designed on KiCad can be used to produce this version of the Raspberry Pi uHAT. 

On printing the board, the final size should be 65x30mm. 

The holes drilled must be no more than 2.75mm.

The [BOM](https://gitlab.com/beemolai12/eee3088f-pihat-project-g14/-/blob/main/PCB/BOM_PCB.csv) lists all the components that will be required.

