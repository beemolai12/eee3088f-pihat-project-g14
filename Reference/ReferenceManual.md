# Reference Manual

### Overview
The microHat board will be connected to a Raspberry Pi Zero for full functionality, and will be used to power a servo motor.

### Electrical Specifications
Vin(max)=12V DC
Iin(max)=13A
Vout(pins 2, 4 and GND)=5V
Vout-max(pins)=3.3V

### Pi Zero GPIO pins to be configured 

- Pulse-Width-Modulated (PWM) input signal to Power Supply Unit – GPIO4
    - Pi Zero configured to generate suitable PWM
- LED pins - GPIO17, GPIO22, GPIO27
    - Max output from Pi = 3.3V
- Amplifier output connected to GPIO16
