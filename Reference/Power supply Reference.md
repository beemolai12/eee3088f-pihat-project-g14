The primary function of this circuit will be to provide a regulated voltage supply for the Pi circuit board. The output voltage can be varied by changing the frequency or duty cycle of the pulse at the base of the transistor in this regulator. This can be achieved using the on board crystal oscillator clock and will be also monitored by the Pi. The circuit comprises of:


- A FET AO6408 transistor( Vishay)
- A 143uH inductor (Choke Manufacturers)
- A 200uH capacitor (Rubycon)
- A load resistance (PiHat)
- A MBR745 Zener Diode (Diodes Inc)

##### Some notes
- Set the Vccin to 12V
- Measure the current using an ammeter and ensure that Iinmax = 13A
- Ensure that Vout = 5V
- Connect Pin 2 of the transistor Q1 to GPIO4
