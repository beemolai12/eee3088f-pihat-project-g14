The speed feedback circuit is connected through a buffer to GPIO 16 which is configured to analogue mode. The aim of this was to keep voltage supplied to the motor at a constant value to maintain its speed and torque regardless of voltage input changes due to unforeseen parameters such as noise, pulsations, changes in motor load. The op-amp used was the IC 741. With the use of a single supply voltage, that may be up to 30V DC, the non-inverting input is supplied with the controlled voltage from the 10k potentiometer.

The op-amp is then used like a voltage follower; the output current is then increased by a series transistor; a BD139. The voltage from the transistor emitter is then fed back into the inverting input of the op amp. Since the op-amp is in negative feedback, it will maintain the balance between its two inputs. The voltage that is applied to the op amp’s non inverting input will be equal to the inverting input, hence ensuring that a constant voltage across the motor is maintained. With this configuration, the speed and direction of the motor will be maintained independent of the motor’s mechanical load variation.

If the motor load increases, the motor armature will draw more current, causing the voltage at the op amp’s inverting input to drop, thus increasing the 741’s voltage output;this in turn counters the effect of the increase in the motor’s load. The BD139 was chosen because it is able to handle 8W of power, The maximum collector current is 1 Amp and the max collector-emitter voltage is 80V. Alternatively, some power transistors like the TIP31C can also be used. The 1N4002 diode rectifier helps to dissipate any back EMF that comes from the motor in case of power supply disconnection.

HOW TO CONNECT the amplifier circuit:

- Connect the input of the amplifier to the power supply
- Make sure the motor is not connected at this instant
- Connect the output of the amplifier circuit GPIO16 ( this ensures the amplifier is connected to the motor through this pin)
- Adjust the potentiometer at the op amps input to ensure that you get the desired output voltage and current.

