The piHat will drive a motor that will be controlling a robotic arm. 

- Each time there is a need for change in position (eg. rotation),  the signal sent to the microcontroller will result in the blue LED switching on.
- When the speed is too high, or the arm direction needs to change, the red LED will switch on.
- When the position is correctly aligned, the green LED will switch on, and will remain on until there is a change that needs to take place.
