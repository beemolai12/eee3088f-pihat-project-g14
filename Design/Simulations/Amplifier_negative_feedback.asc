Version 4
SHEET 1 880 680
WIRE 16 32 -128 32
WIRE 192 32 16 32
WIRE 416 32 192 32
WIRE 608 32 416 32
WIRE 416 128 416 32
WIRE 192 144 192 32
WIRE 608 144 608 32
WIRE 16 160 16 32
WIRE 160 160 128 160
WIRE 352 176 224 176
WIRE 112 192 96 192
WIRE 160 192 112 192
WIRE 416 240 416 224
WIRE 768 240 416 240
WIRE 128 272 128 160
WIRE 272 272 128 272
WIRE 416 272 416 240
WIRE 416 272 272 272
WIRE 416 288 416 272
WIRE 496 288 416 288
WIRE 272 320 272 272
WIRE 416 320 416 288
WIRE 416 432 416 400
WIRE 496 432 496 352
WIRE 496 432 416 432
WIRE 16 448 16 240
WIRE 192 448 192 208
WIRE 192 448 16 448
WIRE 272 448 272 384
WIRE 272 448 192 448
WIRE 416 448 416 432
WIRE 416 448 272 448
WIRE 608 448 608 208
WIRE 608 448 416 448
WIRE 272 496 272 448
FLAG 272 496 0
FLAG -128 112 0
FLAG 768 240 Vout
FLAG 112 192 Vin
SYMBOL Opamps\\UniversalOpamp2 192 176 R0
SYMATTR InstName U1
SYMBOL npn 352 128 R0
SYMATTR InstName Q1
SYMBOL diode 288 384 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMBOL cap 480 288 R0
SYMATTR InstName C1
SYMATTR Value 100n
SYMBOL cap 592 144 R0
SYMATTR InstName C2
SYMATTR Value 470�
SYMBOL voltage -128 16 R0
WINDOW 0 -32 12 Left 2
SYMATTR InstName V1
SYMATTR Value 30
SYMBOL my_pot 32 144 M0
WINDOW 39 48 44 Left 0
SYMATTR SpiceLine R=10k Val =50
SYMATTR InstName X1
SYMBOL res 400 304 R0
WINDOW 0 -66 28 Left 2
SYMATTR InstName Motor
SYMATTR Value 1.2
TEXT -160 520 Left 2 !.tran 1s
